
// WS_Parser.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CWS_ParserApp:
// See WS_Parser.cpp for the implementation of this class
//

class CWS_ParserApp : public CWinApp
{
public:
	CWS_ParserApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CWS_ParserApp theApp;